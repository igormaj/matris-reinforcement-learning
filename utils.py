import pygame
import os
import pickle
from pathlib import Path

class DummySound:

    def play(self):
        return None

def get_sound(filename):
    # return pygame.mixer.Sound(os.path.join(os.path.dirname(__file__), "resources", filename))
    return DummySound()

def filter_dict(dictObj, callback):
    newDict = dict()
    # Iterate over all the items in dictionary
    for (key, value) in dictObj.items():
        # Check if item satisfies the given condition then add to new dict
        if callback((key, value)):
            newDict[key] = value
    return newDict


def create_dirs(path):
    Path(path).parent.mkdir(parents=True, exist_ok=True)


def load_agent(path):
    file = open(path,"rb")
    agent = pickle.load(file)
    file.close()
    return agent


def save_agent(agent, path):
    create_dirs(path=path)
    file = open(path, "wb")
    pickle.dump(agent, file)
    file.close()


class SurfaceDeepCopy:

    def __init__(self, surface):
        self.surface = surface

    def copy(self):

        if not self.surface:
            return None
        surface_size = self.surface.get_size()
        surface_str = pygame.image.tostring(self.surface, "RGB")
        return pygame.image.fromstring(surface_str, surface_size, "RGB")


class SimulatedEvent:

    def __init__(self, type, key):
        self.type = type
        self.key = key
