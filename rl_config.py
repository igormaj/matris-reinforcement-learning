import yaml
from sys import argv

def value_or_default(value, default_value):
    if value is None:
        return default_value
    return value


class Default:
    load_agent = False
    agent_file_path = 'data/q_agent.dat'
    save_agent_after_training = True
    agent_type = 'q_agent'
    LEARNING_RATE = 0.95
    DISCOUNT = 0.95
    EPISODES = 40000
    SHOW_EVERY = 500  # every x episode let us know if you are alive ;)
    GAME_OVER_VALUE = -1000000

    epsilon = 0.5
    START_EPSILON_DECAYING = 1
    END_EPSILON_DECAYING = EPISODES // 2


class Config:

    def __init__(self,
                 load_agent=False,
                 agent_file_path='q_agent.dat',
                 save_agent_after_training=True,
                 agent_type='q_agent',
                 learning_rate=0.95,
                 discount=0.95,
                 episodes=40000,
                 show_every=500,  # every x episode let us know if you are alive ;)
                 game_over_value=-1000000,
                 epsilon=0.5,
                 start_epsilon_decaying=1,
                 end_epsilon_decaying=40000 // 2):

        self.load_agent = value_or_default(load_agent, Default.load_agent)
        self.agent_file_path = value_or_default(agent_file_path, Default.agent_file_path)
        self.save_agent_after_training = value_or_default(save_agent_after_training,
                                                          Default.save_agent_after_training)
        self.agent_type = value_or_default(agent_type,Default.agent_type)
        self.learning_rate = value_or_default(learning_rate, Default.LEARNING_RATE)
        self.discount = value_or_default(discount, Default.DISCOUNT)
        self.episodes = value_or_default(episodes, Default.EPISODES)
        self.show_every = value_or_default(show_every, Default.SHOW_EVERY)
        self.game_over_value = value_or_default(game_over_value, Default.GAME_OVER_VALUE)

        self.epsilon = value_or_default(epsilon, Default.epsilon)
        self.start_epsilon_decaying = value_or_default(start_epsilon_decaying,
                                                       Default.START_EPSILON_DECAYING)
        self.end_epsilon_decaying = value_or_default(end_epsilon_decaying, Default.END_EPSILON_DECAYING)

    def yaml(self):
        return yaml.dump(self.__dict__)

    @staticmethod
    def load(data):
        values = yaml.safe_load(data)
        config = Config()
        for key in values:
            if values[key] is not None:
                config.__dict__[key] = values[key]

        return config


def load_config(path):
    file = open(path, 'r')
    loaded_config = Config.load(file.read())
    file.close()
    return loaded_config


try:
    path = argv[1]
except IndexError:
    path = 'config.yaml'

rl_config = load_config(path) # will be imported wherever needed
