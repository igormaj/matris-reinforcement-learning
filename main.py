#!/usr/bin/env python
import kezmenu
from ai.reinforcement.reinforce.train_reinforce_agent import train_reinforce_agent, reinforce_agent
from rl_config import rl_config
from ai.reinforcement.gd_learning.train_gd_agent import train_gd_agent, matris_to_vect_gd, gd_agent
from ai.reinforcement.q_learning.deep.train_deep_q_agent import train_deep_q_agent, deep_q_agent, matris_to_vect_deep
from ai.reinforcement.q_learning.train_double_q_agent import train_double_q_agent, double_q_agent
from ai.random.random_agent import RandomAgent
import pygame

from ai.reinforcement.q_learning.train_q_agent import train_q_agent, q_agent
from enviroment import Game, WIDTH, HEIGHT, construct_nightmare

from scores import load_score


def get_option(train_options):
    if rl_config.agent_type in train_options:
        return train_options[rl_config.agent_type]
    raise Exception(f"Invalid agent type: '{rl_config.agent_type}'")


def init_train_options():
    train_options = {
        'q_agent': ['Train Q agent!', lambda: train_q_agent(Game(), screen)],
        'double_q_agent': ['Train Double Q agent!', lambda: train_double_q_agent(Game(), screen)],
        'gd_agent': ['Train GD agent!', lambda: train_gd_agent(Game(), screen)],
        'deep_q_agent': ['Train Deep Q agent!', lambda: train_deep_q_agent(Game(), screen)],
        'reinforce_agent': ['Train REINFORCE agent!', lambda: train_reinforce_agent(Game(), screen)],
    }
    return train_options


def init_test_options():
    test_options = {
        'q_agent': ['Test Q agent!', lambda: Game().ai_loop(screen, q_agent)],
        'double_q_agent': ['Test Double Q agent!',
                           lambda: Game().ai_loop(screen, double_q_agent)],
        'gd_agent': ['Test GD Agent', lambda: Game().ai_loop(screen, gd_agent,
                                                             matris_to_vector=matris_to_vect_gd)],
        'deep_q_agent': ['Test Deep Q Agent', lambda: Game().ai_loop(screen, deep_q_agent,
                                                                     matris_to_vector=matris_to_vect_deep)],
        'reinforce_agent': ['Test REINFORCE Agent', lambda: Game().ai_loop(screen, reinforce_agent,
                                                                     matris_to_vector=matris_to_vect_deep)]
    }
    return test_options


class Menu(object):
    """
    Creates main menu
    """
    running = True

    def main(self, screen):
        clock = pygame.time.Clock()
        train_options = init_train_options()
        test_options = init_test_options()
        menu = kezmenu.KezMenu(
            ['Play!', lambda: Game().main(screen)],
            ['Random agent!', lambda: Game().ai_loop(screen, RandomAgent())],
            get_option(train_options),
            get_option(test_options),
            ['Quit', lambda: setattr(self, 'running', False)],
        )
        menu.position = (50, 50)
        menu.enableEffect('enlarge-font-on-focus', font=None, size=60, enlarge_factor=1.2, enlarge_time=0.3)
        menu.color = (255, 255, 255)
        menu.focus_color = (40, 200, 40)

        nightmare = construct_nightmare(screen.get_size())
        highscoresurf = self.construct_highscoresurf()  # Loads highscore onto menu

        timepassed = clock.tick(30) / 1000.

        while self.running:
            events = pygame.event.get()

            for event in events:
                if event.type == pygame.QUIT:
                    exit()

            menu.update(events, timepassed)

            timepassed = clock.tick(30) / 1000.

            if timepassed > 1:  # A game has most likely been played
                highscoresurf = self.construct_highscoresurf()

            screen.blit(nightmare, (0, 0))
            screen.blit(highscoresurf, highscoresurf.get_rect(right=WIDTH - 50, bottom=HEIGHT - 50))
            menu.draw(screen)
            pygame.display.flip()

    def construct_highscoresurf(self):
        """
        Loads high score from file
        """
        font = pygame.font.Font(None, 50)
        highscore = load_score()
        text = "Highscore: {}".format(highscore)
        return font.render(text, True, (255, 255, 255))


if __name__ == '__main__':
    pygame.init()

    screen = pygame.display.set_mode((WIDTH, HEIGHT))
    pygame.display.set_caption("MaTris")
    Menu().main(screen)
