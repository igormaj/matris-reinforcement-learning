from abc import ABC, abstractmethod


class AbstractAgent(ABC):

    """ Every agent inheriting this class needs to implement this method"""
    @abstractmethod
    def next_action(self,state):
        pass