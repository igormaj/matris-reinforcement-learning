"""
    Methods that help convert the game state to vector representation
"""
from copy import deepcopy

import numpy as np

from matris import MATRIX_HEIGHT, MATRIX_WIDTH
from tetrominoes import list_of_tetrominoes


class DummyMatris:
    """ For vector extraction purposes (without unnecessary baggage) """

    def __init__(self):
        self.matrix = {}


def get_height(matris):
    max_height = 0
    for column_index in range(0, MATRIX_WIDTH):
        col_height = get_column_height(matris, column_index)
        if col_height > max_height:
            max_height = col_height

    return max_height


def is_hole(matris, x, y):
    """Number of holes in the board (empty sqquare with at least one block above it)"""
    try:
        return matris.matrix[(y, x)] is None \
               and matris.matrix[(y - 1, x)] is not None
    except KeyError:
        return False


# Number of holes in the board (empty square with at least one block above it)
def get_num_holes(matris):
    # first two rows are hidden, which is convenient for this hole detection algorithm
    num_holes = 0
    for y in range(1, MATRIX_HEIGHT):
        for x in range(MATRIX_WIDTH):
            if is_hole(matris, x, y):
                num_holes += 1

    return num_holes


def get_column_height(matris, col_index):
    height = 0
    for i in range(MATRIX_HEIGHT):
        # we go from the top of the matrix (selected column only)
        # when we find the first non empty field (highest)
        # we calculate the column height and exit the loop
        if matris.matrix[(i, col_index)] is not None:
            height = MATRIX_HEIGHT - i
            break

    return height


# Bumpiness (sum of the difference between heights of adjacent pairs of columns)
def get_bumpiness(matris):
    bumpiness = 0
    for column_index in range(1, MATRIX_WIDTH):
        column1 = get_column_height(matris, column_index - 1)
        column2 = get_column_height(matris, column_index)
        bumpiness += abs(column2 - column1)

    return bumpiness


def get_sum_height(matris):
    height_sum = 0
    for column_index in range(MATRIX_WIDTH):
        column1 = get_column_height(matris, column_index)
        height_sum += column1

    return height_sum


def get_lines_cleared(matris):
    return matris.current_lines_cleared


def get_column_heights(matris):
    ret_val = []
    for column_index in range(MATRIX_WIDTH):
        ret_val.append(get_column_height(matris, column_index))

    return tuple(ret_val)


def get_uneveness_factor(matris):
    """Calculates column heights and outputs
       sum which says how much does each column deviate from the average"""

    column_heights = np.array(get_column_heights(matris))
    average_column_heights = np.array([np.average(column_heights)] * len(column_heights))

    return np.sum((average_column_heights - column_heights) ** 2)


def get_esann_cell(height):
    """Returns the cell (1-5) from a given column height
       Each esann cell consists of four normal cell"""
    if height <= 4:
        return 1
    elif height <= 8:
        return 2
    elif height <= 12:
        return 3
    elif height <= 16:
        return 4
    return 5


def imprint_tetromino_on_board(matris):
    """Finds out tetromino position as it falls and fills out
       the squares on the matrix"""
    ret_val = deepcopy(matris)
    new_matrix = matris.blend()
    if not new_matrix:
        new_matrix = matris.matrix

    ret_val.matrix = new_matrix

    return ret_val


def get_esann_vector(matris):
    """column height with 5 possible states per column (1-5)"""
    ret_val = []
    for column_height in get_column_heights(matris):
        ret_val.append(get_esann_cell(column_height))
    return tuple(ret_val)


"""Converts the matris object to a feature vector that can be used to index
   q table
   Format:  
   """


def matris_to_vector(matris):
    return get_esann_vector(matris)
    # vector = get_esann_vector(matris)
    # vector.append(get_sum_height(matris))
    # vector.append(get_lines_cleared(matris))
    # vector.append(get_num_holes(matris))
    # vector.append(get_bumpiness(matris))
    # vector.append(list_of_tetrominoes.index(matris.current_tetromino))
    # vector.append(matris.tetromino_position)
    # vector.append(matris.tetromino_rotation)
    # return tuple(vector)


def get_gd_vector(matris):
    """Returns tuple which contains
     height_sum, diff_sum, max_height, holes"""
    column_heights = get_column_heights(matris)

    holes = get_num_holes(matris)
    height_sum = sum(column_heights)
    diff_sum = 0
    max_height = max(column_heights)

    for i in range(1, len(column_heights)):
        diff_sum += abs(column_heights[i] - column_heights[i - 1])

    return height_sum, diff_sum, max_height, holes


def get_deep_vector(matris):
    column_heights = get_column_heights(matris)

    holes = get_num_holes(matris)
    height_sum = sum(column_heights)
    diff_sum = 0
    lines = get_lines_cleared(matris)

    for i in range(1, len(column_heights)):
        diff_sum += abs(column_heights[i] - column_heights[i - 1])

    return height_sum, diff_sum, lines, holes


def get_column_diffs(matris):
    column_heights = get_column_heights(matris)
    ret_val = []
    for i in range(1, len(column_heights)):
        ret_val.append(column_heights[i] - column_heights[i-1])
    return ret_val


def get_reinforce_vector(matris):
    """return np.asarray(get_column_heights(matris) + \
                      (get_tetromino_index(matris.current_tetromino),
                       get_tetromino_index(matris.next_tetromino),
                       get_num_holes(matris)))
    #return np.asarray(get_column_diffs(matris))"""
    return get_matrix(matris)


def get_tetromino_index(tetromino):
    return list_of_tetrominoes.index(tetromino)


# Gets the matrix as np matrix
# takes into account the falling tetromino position
def get_matrix(matris):
    tups = imprint_tetromino_on_board(matris).matrix
    matrix_lst = [[0 for i in range(MATRIX_WIDTH)] for j in range(MATRIX_HEIGHT)]

    for y in range(MATRIX_HEIGHT):
        for x in range(MATRIX_WIDTH):
            if tups[(y, x)] is not None:
                matrix_lst[y][x] = 1

    ret_val = np.asarray(matrix_lst)
    #print(ret_val)
    return ret_val
