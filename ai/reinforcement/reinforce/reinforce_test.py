import gym
import numpy as np
from keras import Input, Model
from keras.layers import Dense
from keras.optimizers import Adam
import keras.backend as K

env = gym.make('LunarLander-v2')

INPUT_SIZE = env.observation_space.shape[0]
OUTPUT_SIZE = env.action_space.n
alpha = 0.0005
gamma = 0.99
EPISODES = 10000


def discount_rewards(rewards):
    discounted_rewards = np.zeros_like(rewards)
    running_add = 0
    for t in reversed(range(0, len(rewards))):
        running_add = running_add * gamma + rewards[t]
        discounted_rewards[t] = running_add
    return discounted_rewards


def build_policy_network():
    input = Input(shape=(INPUT_SIZE,))
    layer = Dense(24, activation='relu',
                    kernel_initializer='glorot_uniform')(input)
    for i in range(2):
        layer = Dense(24, activation='relu',
                    kernel_initializer='glorot_uniform')(layer)
    output = Dense(OUTPUT_SIZE, activation='softmax')(layer)
    # template,will be replaced with actual
    # discounted rewards
    advantages = Input(shape=[1])

    def custom_loss(y_true,y_pred):
        # we use the clip function, because
        # we do not want to deal with log(0)
        # when calculating log loss
        out = K.clip(y_pred, 1e-8, 1-1e-8)
        log_likelihood = y_true*K.log(out)
        return K.sum(-log_likelihood * advantages)

    # Advantage tell us the value of a reward if we take
    # a particular action at a given time step
    policy = Model(inputs=[input, advantages], outputs=[output])
    policy.compile(optimizer=Adam(alpha),
                   loss=custom_loss)

    predict = Model(inputs=[input],outputs=[output])
    return policy, predict


class ReinforceAgent:

    def __init__(self):
        self.policy_network, self.predictor = \
            build_policy_network()

        self.states = []
        self.actions = []
        self.rewards = []

        self.action_space = [i for i in range(OUTPUT_SIZE)]

    def next_action(self, state):
        # this converts a single observation
        # into a batch of one element
        states = state[np.newaxis, :]
        probabilities = self.predictor.predict(states)[0]
        action = np.random.choice(self.action_space,p=probabilities)
        return action

    def reset_memory(self):
        self.states = []
        self.actions = []
        self.rewards = []

    def remember(self, state, action, reward):
        self.states.append(state)
        self.actions.append(action)
        self.rewards.append(reward)

    def train(self):
        self.states = np.asarray(self.states)
        self.actions = np.asarray(self.actions)
        self.rewards = np.asarray(self.rewards)

        # We have to take the actions and turn them
        # into one-hot encoded vectors
        actions_one_hot = np.zeros([len(self.actions), OUTPUT_SIZE])
        actions_one_hot[np.arange(len(self.actions)), self.actions] = 1

        discount_r = discount_rewards(self.rewards)
        self.policy_network.train_on_batch([self.states, discount_r], actions_one_hot)

        self.reset_memory()


if __name__ == '__main__':
    agent = ReinforceAgent()
    scores, episodes = [], []

    for e in range(EPISODES):
        done = False
        score = 0
        state = env.reset()
        while not done:
            if e % 100 == 0:
                env.render()

            # get action for the current state and go one step in environment
            action = agent.next_action(state)

            next_state, reward, done, info = env.step(action)
            # save the sample <s, a, r> to the memory
            agent.remember(state, action, reward)

            score += reward
            state = next_state

            if done:
                # every episode, agent learns from sample returns
                agent.train()

                # every episode, plot the play time
                scores.append(score)
                episodes.append(e)
                print("episode:", e, "  score:", score)
