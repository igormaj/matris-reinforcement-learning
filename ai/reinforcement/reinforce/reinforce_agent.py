import numpy as np
from keras import Input, Model
from keras.layers import Dense, Flatten, Conv2D
from keras.optimizers import Adam

from ai.abstract_agent import AbstractAgent
from ai.actions import all_actions
from ai.reinforcement.super_moves import all_super_moves, super_move_to_actions, POSSIBLE_MOVES_NUMBER
from ai.reinforcement.vectorization import get_reinforce_vector
from matris import MATRIX_HEIGHT, MATRIX_WIDTH
from rl_config import rl_config
import keras.backend as K
import keras.losses

# INPUT_SIZE = 13  # equal to feature vector size
OUTPUT_SIZE = len(all_super_moves)


def discount_rewards(rewards):
    discounted_rewards = np.zeros_like(rewards)
    running_add = 0
    for t in reversed(range(0, len(rewards))):
        running_add = running_add * rl_config.discount + rewards[t]
        discounted_rewards[t] = running_add
    return discounted_rewards


# Keras needs this format for input into CNN (for single channel images)
def reshape_for_cnn(states):
    return states.reshape(states.shape[0],
                          states.shape[1],
                          states.shape[2], 1)


# Advantage tell us the value of a reward if we take
# a particular action at a given time step
# template,will be replaced with actual
# discounted rewards
advantages = Input(shape=[1])


def custom_loss(y_true, y_pred):
    # we use the clip function, because
    # we do not want to deal with log(0)
    # when calculating log loss
    out = K.clip(y_pred, 1e-8, 1 - 1e-8)
    log_likelihood = y_true * K.log(out)
    return K.sum(-log_likelihood * advantages)


# needed for the model to load correctly
keras.losses.custom_loss = custom_loss


def build_policy_network():
    input = Input(shape=(MATRIX_HEIGHT, MATRIX_WIDTH, 1))
    layer = Conv2D(filters=32, kernel_size=(2, 2), strides=(1, 1), activation='relu')(input)
    for i in range(2):
        layer = Conv2D(filters=64, kernel_size=(2, 2), strides=(1, 1), activation='relu')(layer)
    layer = Flatten()(layer)
    layer = Dense(units=128, activation='relu')(layer)
    output = Dense(OUTPUT_SIZE, activation='softmax')(layer)

    policy = Model(inputs=[input, advantages], outputs=[output])
    policy.compile(optimizer=Adam(rl_config.learning_rate),
                   loss=custom_loss)

    policy.summary()
    predict = Model(inputs=[input], outputs=[output])
    return policy, predict


class ReinforceAgent(AbstractAgent):

    def __init__(self):
        self.policy_network, self.predictor = \
            build_policy_network()
        self.states = []
        self.actions = []
        self.rewards = []
        self.action_space = [i for i in range(OUTPUT_SIZE)]

        self.last_taken_action_index = None

    def next_action(self, state):
        # this converts a single observation
        # into a batch of one element
        states = get_reinforce_vector(state)[np.newaxis, :]
        states = reshape_for_cnn(states)

        probabilities = self.predictor.predict(states)[0]
        action_index = np.random.choice(self.action_space, p=probabilities)
        self.last_taken_action_index = action_index

        return super_move_to_actions(all_super_moves[action_index])

    def reset_memory(self):
        self.states = []
        self.actions = []
        self.rewards = []

    def remember(self, state, action, reward):
        self.states.append(state)
        self.actions.append(action)
        self.rewards.append(reward)

    def train(self):
        self.states = np.asarray(self.states)
        # Reshaping to (num_elements, height,width,1) -> for CNN
        self.states = reshape_for_cnn(self.states)

        self.actions = np.asarray(self.actions)
        self.rewards = np.asarray(self.rewards)

        # We have to take the actions and turn them
        # into one-hot encoded vectors
        actions_one_hot = np.zeros([len(self.actions), OUTPUT_SIZE])
        actions_one_hot[np.arange(len(self.actions)), self.actions] = 1

        discount_r = discount_rewards(self.rewards)
        self.policy_network.train_on_batch([self.states, discount_r], actions_one_hot)

        self.reset_memory()
