from statistics import mean

import numpy as np

from ai.reinforcement.vectorization import get_column_heights, get_num_holes, get_reinforce_vector, get_gd_vector, \
    imprint_tetromino_on_board
from matris import MATRIX_WIDTH, MATRIX_HEIGHT
from rl_config import rl_config


def custom_reward_function(new_state, base_reward=0):
    column_heights = get_column_heights(new_state)
    avg_height = mean(value for value in column_heights)
    quadratic_unevenness = 0
    for i in range(len(column_heights) - 1):
        quadratic_unevenness += (column_heights[i + 1] - column_heights[i]) ** 2
    return - (avg_height * 5 + get_num_holes(new_state) * 16 + quadratic_unevenness * 1) + 10 * base_reward


def clear_line_reward_function(new_vector, old_vector, test_lines_removed):
    height_sum, diff_sum, max_height, holes = new_vector
    reference_height = old_vector[0]
    return 5 * (test_lines_removed ** 2) - (height_sum - reference_height)


def clear_line_reward_function_deep(new_vector, old_vector, test_lines_removed, done):
    if done:
        return rl_config.game_over_value

    height_sum, diff_sum, max_height, holes = new_vector
    reference_height = old_vector[0]
    return 100 * test_lines_removed ** 2 - 5 * (height_sum - reference_height) - 10 * holes


# should get the number of empty fields in row
def get_empty_field_count(matris, row_index):
    count = 0
    for x in range(MATRIX_WIDTH):
        if matris.matrix[(MATRIX_HEIGHT - 1 - row_index, x)] is None:
            count += 1
    return count


def clear_line_reward_function_reinforce(new_state, current_state, done=False):

    current_heights = get_column_heights(current_state)
    new_heights = get_column_heights(new_state)

    current_avg_height = np.sum(current_heights)
    new_avg_height = np.sum(new_heights)
    current_holes = get_num_holes(current_state)
    next_holes = get_num_holes(new_state)

    quadratic_unevenness1 = 0
    for i in range(len(current_heights) - 1):
        quadratic_unevenness1 += (current_heights[i + 1] - current_heights[i]) ** 2

    quadratic_unevenness2 = 0
    for i in range(len(new_heights) - 1):
        quadratic_unevenness2 += (new_heights[i + 1] - new_heights[i]) ** 2

    return 10*(new_state.current_lines_cleared**2) + 5*(current_avg_height - new_avg_height) + 16*(current_holes - next_holes) + (quadratic_unevenness1 -
                                                                                        quadratic_unevenness2)
