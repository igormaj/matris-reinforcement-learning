import math
from copy import deepcopy

import pygame

from ai.abstract_agent import AbstractAgent
from ai.reinforcement.rewards import clear_line_reward_function
from ai.reinforcement.super_moves import super_move_to_actions, rotation_range, columns_range
from ai.reinforcement.vectorization import get_gd_vector
from exceptions import GameOver
from rl_config import rl_config
from utils import SimulatedEvent


def get_expected_score(matris, weights):
    # This function calculates the score of a given board state, given weightss
    vector = get_gd_vector(matris)
    test_score = 0.0
    for i in range(len(weights)):
        test_score += weights[i] * vector[i]
    return test_score


class GDAgent(AbstractAgent):
    """ Gradient descent agent (offline learning) """

    def __init__(self):
        super(GDAgent, self).__init__()
        self.weights = [-1, -1, -1, -60]
        self.v = [0]*len(self.weights) # keeping momentum in gd with momentum
        self.rotation_range = rotation_range()
        self.columns_range = columns_range()

    def next_action(self, matris):
        """
           Calculates the best super move (rotations,columns)
           This move is then mapped to individual actions, which are
           done in a single frame
        """
        best_actions = None
        max_score = None
        for rotation in self.rotation_range:
            for sideways in self.columns_range:
                possible_actions = super_move_to_actions((rotation, sideways))
                new_state, _, _, _ = self.simulate_step(matris, possible_actions)
                expected_score = get_expected_score(new_state, self.weights)
                if max_score is None:
                    max_score = expected_score
                    best_actions = possible_actions

                if expected_score > max_score:
                    max_score = expected_score
                    best_actions = possible_actions

        return best_actions

    def gradient_descent_with_momentum(self, matris, best_action):
        """ This method takes the state and  updates the weights"""
        alpha = rl_config.learning_rate
        beta = 0.9
        gamma = rl_config.discount
        old_params = get_gd_vector(matris)
        new_state, one_step_reward, done, _ = self.simulate_step(matris, best_action)
        new_params = get_gd_vector(new_state)

        for i in range(0, len(self.weights)):
            gradient = (- self.weights[i]) * (one_step_reward - old_params[i] + gamma * new_params[i])
            self.v[i] = beta*self.v[i] + (1-beta)*gradient
            self.weights[i] = self.weights[i] - alpha * self.v[i]

        # Regularization so the weights do not explode
        self._regularization()

    def _regularization(self):
        regularization_term = abs(sum(self.weights))
        for i in range(0, len(self.weights)):
            self.weights[i] = 100 * self.weights[i] / regularization_term
            self.weights[i] = math.floor(1e4 * self.weights[i]) / 1e4  # Rounds the weights

    def gradient_descent(self, matris, best_action):
        """ This method takes the state and updates the weights"""
        alpha = rl_config.learning_rate
        gamma = rl_config.discount
        old_params = get_gd_vector(matris)
        new_state, one_step_reward, done, _ = self.simulate_step(matris, best_action)
        new_params = get_gd_vector(new_state)

        for i in range(0, len(self.weights)):
            self.weights[i] = self.weights[i] + alpha * self.weights[i] * (
                    one_step_reward - old_params[i] + gamma * new_params[i])
        # Regularization so the weights do not explode
        self._regularization()

    def simulate_step(self, matris, action):
        """Simulates one step in a game loop
           returns new_state, reward, done, additional data
        """
        old_vector = get_gd_vector(matris)
        matris = deepcopy(matris)
        done = False
        try:
            timepassed = 50
            events = self._simulate_key_press(action=action)
            matris.update((timepassed / 1000.), events)
        except GameOver:
            done = True

        test_lines_removed = matris.current_lines_cleared
        new_vector = get_gd_vector(matris)
        reward = clear_line_reward_function(new_vector, old_vector, test_lines_removed)
        return matris, reward, done, None

    @staticmethod
    def _simulate_key_press(action):
        if action is None:
            return []
        if type(action) is list:
            return [SimulatedEvent(type=pygame.KEYDOWN, key=act) for act in action]

        return [SimulatedEvent(type=pygame.KEYDOWN, key=action)]