from ai.reinforcement.gd_learning.gd_agent import GDAgent
from datetime import datetime
import numpy as np

from ai.reinforcement.super_moves import get_random_actions
from rl_config import rl_config
from utils import save_agent, load_agent

if rl_config.load_agent and rl_config.agent_type == 'gd_agent':
    gd_agent = load_agent(rl_config.agent_file_path)
else:
    gd_agent = GDAgent()


def matris_to_vect_gd(matris):
    return matris


def train_gd_agent(env, screen):
    max_lines_cleared = 0
    epsilon = rl_config.epsilon * 1
    epsilon_decay_value = epsilon / (rl_config.end_epsilon_decaying - rl_config.start_epsilon_decaying)

    for episode in range(rl_config.episodes):
        new_state = env.reset(screen=screen)
        episode_reward = 0
        done = False
        if episode % rl_config.show_every == 0:
            render = True
        else:
            render = False
        now = datetime.now()
        while not done:

            if np.random.random() > epsilon:
                action = gd_agent.next_action(new_state)
            else:
                action = get_random_actions()

            gd_agent.gradient_descent_with_momentum(new_state,action) # update weights
            new_state, reward, done, _ = env.step(action, render)

            episode_reward += reward
            if render:
                env.render()

        if rl_config.end_epsilon_decaying >= episode >= rl_config.start_epsilon_decaying:
            epsilon -= epsilon_decay_value

        print("-" * 30)
        print(f"Episode: {episode}")
        print(f"Episode reward: {episode_reward}")
        print(f"Epsilon: {epsilon}")
        print(f"Episode done in: {datetime.now() - now}")
        print(f"Lines cleared: {new_state.lines}")
        print(f"Weigths:  {gd_agent.weights}")
        print("-" * 30)
        if new_state.lines > max_lines_cleared:
            max_lines_cleared = new_state.lines
            save_agent(path=rl_config.agent_file_path,agent=gd_agent)

    if rl_config.save_agent_after_training:
        save_agent(path=rl_config.agent_file_path, agent=gd_agent)

    env.close()
