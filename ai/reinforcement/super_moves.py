import random
from ai.actions import Actions


def super_move_to_actions(super_move):
    """
       Super move(rotations, sideways)
       This functions maps the super move
       to individual actions
    """
    action_list = []
    rotations = super_move[0]
    sideways = super_move[1]
    for i in range(rotations):
        action_list.append(Actions.UP)

    while sideways != 0:
        if sideways < 0:
            action_list.append(Actions.LEFT)
            sideways += 1
        elif sideways > 0:
            action_list.append(Actions.RIGHT)
            sideways -= 1
    else:
        action_list.append(Actions.HARD_DROP)

    return action_list


def rotation_range(): return range(4)


def columns_range(): return range(-5, 5)


def get_random_actions():
    """Takes a random super move then converts it to basic actions
       for the game engine """
    return super_move_to_actions(random.choice(all_super_moves))


def actions_to_super_move(actions):
    """Takes a list of actions and converts it to a super move"""
    for super_move in all_super_moves:
        if super_move_to_actions(super_move) == actions:
            return super_move

    raise Exception('Could not find supermove!')

all_super_moves = []

for rotation in rotation_range():
    for sideways in columns_range():
        all_super_moves.append((rotation, sideways))

POSSIBLE_MOVES_NUMBER = len(all_super_moves)

if __name__ == '__main__':
    print(f"Moves: {all_super_moves}")
    print(f"Number of possible moves: {POSSIBLE_MOVES_NUMBER}")
