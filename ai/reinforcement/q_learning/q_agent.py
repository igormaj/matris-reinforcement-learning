from ai.abstract_agent import AbstractAgent
from ai.actions import all_actions
import numpy as np


class QTable:
    def __init__(self):
        self.data = {}  # tuple feature vector key, value list of action values

    def _check(self, vector_state):
        if vector_state not in self.data:
            # print("New state discovered!")
            self.data[vector_state] = np.random\
                .uniform(low=-3, high=0, size=len(all_actions)).astype(np.longdouble)

    def get_q_value(self, vector_state, action):
        self._check(vector_state)
        return self.data[vector_state][all_actions.index(action)]

    def get_max_q_value(self, vector_state):
        self._check(vector_state)
        return np.max(self.data[vector_state])

    def set_q_value(self, vector_state, action, new_q_value):
        self._check(vector_state)
        self.data[vector_state][all_actions.index(action)] = new_q_value

    def best_action(self, vector_state):
        self._check(vector_state)
        return all_actions[np.argmax(self.data[vector_state])]

    def __len__(self):
        return len(self.data)


class QAgent(AbstractAgent):
    """ Q learning agent who picks his moves according to the q values table """

    def __init__(self):
        super(QAgent, self).__init__()
        self.q_table = QTable()

    def next_action(self, vector_state):
        action = self.q_table.best_action(vector_state)
        # print("Q agent:", action, action_map[action])
        return action
