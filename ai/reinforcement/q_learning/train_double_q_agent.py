import random

from ai.actions import all_actions
from ai.reinforcement.q_learning.double_q_agent import DoubleQAgent
from ai.reinforcement.rewards import custom_reward_function
from ai.reinforcement.vectorization import matris_to_vector
from datetime import datetime
import numpy as np
from rl_config import rl_config
from utils import load_agent, save_agent


if rl_config.load_agent and rl_config.agent_type == 'double_q_agent':
    double_q_agent = load_agent(rl_config.agent_file_path)
else:
    double_q_agent = DoubleQAgent()


def update_agent(agent, old_vector_state, new_vector_state, reward, a):
    if np.random.random() > 0.5:
        # update A table
        a_star = agent.a_table.best_action(new_vector_state)
        new_q_value = \
            agent.a_table.get_q_value(old_vector_state, a) + \
            rl_config.learning_rate * (reward + rl_config.discount *
                                       agent.b_table.get_q_value(new_vector_state, a_star) -
                                       agent.a_table.get_q_value(old_vector_state, a))
        agent.a_table.set_q_value(old_vector_state, a, new_q_value)
    else:
        # update B table
        b_star = agent.b_table.best_action(new_vector_state)
        new_q_value = \
            agent.b_table.get_q_value(old_vector_state, a) + \
            rl_config.learning_rate * (reward + rl_config.discount *
                                       agent.a_table.get_q_value(new_vector_state, b_star)
                                       - agent.b_table.get_q_value(old_vector_state, a))
        agent.b_table.set_q_value(old_vector_state, a, new_q_value)


def train_double_q_agent(env, screen):
    epsilon = rl_config.epsilon * 1
    epsilon_decay_value = epsilon / (rl_config.end_epsilon_decaying - rl_config.start_epsilon_decaying)

    for episode in range(rl_config.episodes):
        new_state = env.reset(screen=screen)
        vector_state = matris_to_vector(new_state)
        episode_reward = 0
        custom_reward = 0
        done = False
        if episode % rl_config.show_every == 0:
            render = True
        else:
            render = False
        now = datetime.now()
        while not done:

            if np.random.random() > epsilon:
                action = double_q_agent.next_action(vector_state)
            else:
                action = random.choice(all_actions)

            new_state, reward, done, _ = env.step(action, render)

            episode_reward += reward

            old_vector_state = vector_state
            vector_state = matris_to_vector(new_state)
            reward = custom_reward_function(new_state)
            custom_reward += reward
            if render:
                env.render()

            if not done:
                update_agent(double_q_agent, old_vector_state, vector_state, reward, action)
            else:
                # Tetris Gameover

                # update_agent(double_q_agent, old_vector_state, vector_state, rl_config.game_over_value, action)
                double_q_agent.\
                    a_table.set_q_value(vector_state, action, rl_config.game_over_value)
                double_q_agent.\
                    b_table.set_q_value(vector_state, action, rl_config.game_over_value)

        if rl_config.end_epsilon_decaying >= episode >= rl_config.start_epsilon_decaying:
            epsilon -= epsilon_decay_value

        print("-" * 30)
        print(f"Episode: {episode}")
        print(f"Episode reward: {episode_reward}")
        print(f"Custom reward: {custom_reward}")
        print(f"Epsilon: {epsilon}")
        double_q_agent.print_num_explored_states()
        print(f"Episode done in: {datetime.now() - now}")
        print("-" * 30)

        if render:
            save_agent(double_q_agent, rl_config.agent_file_path)

    print(f"Q Table: {double_q_agent.q_table.data}")

    if rl_config.save_agent_after_training:
        save_agent(double_q_agent, rl_config.agent_file_path)

    env.close()
