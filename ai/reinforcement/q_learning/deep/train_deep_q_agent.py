from datetime import datetime
import numpy as np

from ai.reinforcement.q_learning.deep.deep_q_agent import DeepQAgent
from ai.reinforcement.rewards import clear_line_reward_function_deep
from ai.reinforcement.super_moves import get_random_actions, actions_to_super_move
from ai.reinforcement.vectorization import get_deep_vector
from rl_config import rl_config
from utils import save_agent, load_agent

if rl_config.load_agent and rl_config.agent_type == 'deep_q_agent':
    deep_q_agent = load_agent(rl_config.agent_file_path)
else:
    deep_q_agent = DeepQAgent()


def matris_to_vect_deep(matris):
    return matris


def train_deep_q_agent(env, screen):
    max_lines_cleared = 0
    epsilon = rl_config.epsilon * 1
    epsilon_decay_value = epsilon / (rl_config.end_epsilon_decaying - rl_config.start_epsilon_decaying)

    for episode in range(rl_config.episodes):
        new_state = env.reset(screen=screen)
        episode_reward = 0
        done = False
        if episode % rl_config.show_every == 0:
            render = True
        else:
            render = False
        now = datetime.now()
        while not done:

            if np.random.random() > epsilon:
                action = deep_q_agent.next_action(new_state)
            else:
                action = get_random_actions()

            current_state = new_state
            new_state, reward, done, _ = env.step(action, render)

            old_vector = get_deep_vector(current_state)
            new_vector = get_deep_vector(new_state)
            custom_reward = clear_line_reward_function_deep(new_vector, old_vector,new_state.current_lines_cleared, done)
            transition = (old_vector, custom_reward, new_vector, done)
            deep_q_agent.train(transition)  # update weights

            episode_reward += reward
            if render:
                env.render()

        if rl_config.end_epsilon_decaying >= episode >= rl_config.start_epsilon_decaying:
            epsilon -= epsilon_decay_value

        print("-" * 30)
        print(f"Episode: {episode}")
        print(f"Episode reward: {episode_reward}")
        print(f"Epsilon: {epsilon}")
        print(f"Episode done in: {datetime.now() - now}")
        print(f"Lines cleared: {new_state.lines}")
        print("-" * 30)
        if new_state.lines > max_lines_cleared:
            max_lines_cleared = new_state.lines
            save_agent(path=rl_config.agent_file_path,agent=deep_q_agent)

    if rl_config.save_agent_after_training:
        save_agent(path=rl_config.agent_file_path, agent=deep_q_agent)

    env.close()
