import random
from collections import deque
from copy import deepcopy

import numpy as np
import pygame
from keras import Sequential
from keras.layers import Dense
from keras.optimizers import Adam

from ai.abstract_agent import AbstractAgent
from ai.reinforcement.rewards import clear_line_reward_function_deep
from ai.reinforcement.super_moves import all_super_moves, super_move_to_actions
from ai.reinforcement.vectorization import get_deep_vector
from exceptions import GameOver
from rl_config import rl_config
from utils import SimulatedEvent


INPUT_SIZE = 4 # equal to feature vector size
REPLAY_MEMORY_SIZE = 15_000
MIN_REPLAY_MEMORY_SIZE = 200
# MINI_BATCH_SIZE = 512


def create_model(print_summary=False):
    model = Sequential()
    model.add(Dense(64, input_dim=INPUT_SIZE, activation='linear', kernel_initializer='glorot_uniform'))
    model.add(Dense(64, activation='linear', kernel_initializer='glorot_uniform'))
    model.add(Dense(32, activation='linear', kernel_initializer='glorot_uniform'))
    model.add(Dense(1, activation='linear'))

    if print_summary:
        model.summary()
    model.compile(loss='mse', optimizer=Adam(learning_rate=rl_config.learning_rate), metrics=['accuracy'])

    return model


class DeepQAgent(AbstractAgent):
    """ Q learning agent who picks his moves according to the approximations
        given by a neural network"""

    def __init__(self):
        super(DeepQAgent, self).__init__()

        # this model will be fitted every single step
        self.model = create_model(print_summary=True)

        self.replay_memory = deque(maxlen=REPLAY_MEMORY_SIZE)
        self.epochs = 1

    # transition -> (old_vector, action, reward, new_vector, done?)
    def update_replay_memory(self, transition):
        self.replay_memory.append(transition)

    def get_q_value(self, vector_state):
        return self.model.predict(np.asarray([vector_state]))[0][0] # one example, one prediction

    def simulate_step(self, matris, action):
        """Simulates one step in a game loop
           returns new_state, reward, done, additional data
        """
        old_vector = get_deep_vector(matris)
        matris = deepcopy(matris)
        done = False
        try:
            timepassed = 50
            events = self._simulate_key_press(action=action)
            matris.update((timepassed / 1000.), events)
        except GameOver:
            done = True

        new_vector = get_deep_vector(matris)
        reward = clear_line_reward_function_deep(new_vector, old_vector, matris.current_lines_cleared,done)
        return matris, reward, done, None

    @staticmethod
    def _simulate_key_press(action):
        if action is None:
            return []
        if type(action) is list:
            return [SimulatedEvent(type=pygame.KEYDOWN, key=act) for act in action]

        return [SimulatedEvent(type=pygame.KEYDOWN, key=action)]

    def next_action(self, matris):
        """
           Calculates the best super move (rotations,columns)
           This move is then mapped to individual actions, which are
           done in a single frame
        """
        best_actions = None
        max_score = None

        for super_move in all_super_moves:
            possible_actions = super_move_to_actions(super_move)
            new_state, _, _, _ = self.simulate_step(matris, possible_actions)
            expected_score = self.get_q_value(get_deep_vector(new_state))
            if max_score is None:
                max_score = expected_score
                best_actions = possible_actions

            if expected_score > max_score:
                max_score = expected_score
                best_actions = possible_actions

        return best_actions

    def train(self, transition):
        self.update_replay_memory(transition)
        if len(self.replay_memory) < MIN_REPLAY_MEMORY_SIZE:
            # Otherwise we run into overfitting risk
            return

        mini_batch = random.sample(self.replay_memory, len(self.replay_memory))

        # transition -> (old_vector, reward, new_vector, done?)
        # current_states = np.array([transition[0] for transition in mini_batch])
        # current_q_values_list = self.model.predict(current_states)

        new_states = np.array([transition[2] for transition in mini_batch])
        future_q_values_list = np.array([val[0] for val in self.model.predict(new_states)])

        x = [] # feature vectors input -> len = minibatch_size
        y = [] # labels(list of q values) -> len = minibatch_size

        for index, (current_vector, reward, new_vector, done) in enumerate(mini_batch):

            if not done:
                max_future_q = future_q_values_list[index]  # one q value for each example
                new_q = reward + rl_config.discount*max_future_q # main part of the original equation
            else:
                new_q = reward

            # current_q_values = current_q_values_list[index]
            # current_q_values[all_super_moves.index(action)] = new_q

            x.append(current_vector)
            y.append(new_q)

        x = np.asarray(x)
        y = np.asarray(y)
        self.model.fit(x=x, y=y, batch_size=len(self.replay_memory),verbose=1, shuffle=False,
                       epochs=self.epochs,
                       callbacks=[]) # train all at once (one step)
