import numpy as np

from ai.abstract_agent import AbstractAgent
from ai.reinforcement.q_learning.q_agent import QTable


class DoubleQAgent(AbstractAgent):
    """ Double Q learning agent using two q tables to pick his moves """

    def __init__(self):
        super(DoubleQAgent, self).__init__()
        self.a_table = QTable()
        self.b_table = QTable()

    # randomly returns action from table a or table b
    def next_action(self, vector_state):
        if np.random.random() > 0.5:
            return self.a_table.best_action(vector_state)
        return self.b_table.best_action(vector_state)

    def print_num_explored_states(self):
        print("Num explored states: ")
        print(f"A table: {len(self.a_table)}")
        print(f"B table: {len(self.b_table)}")
