import random
from ai.actions import all_actions
from ai.reinforcement.q_learning.q_agent import QAgent
from ai.reinforcement.vectorization import matris_to_vector, \
    get_uneveness_factor, get_height
import numpy as np
from datetime import datetime

from rl_config import rl_config
from utils import load_agent, save_agent

if rl_config.load_agent and rl_config.agent_type == 'q_agent':
    q_agent = load_agent(rl_config.agent_file_path)
else:
    q_agent = QAgent()


def train_q_agent(env, screen):
    epsilon = rl_config.epsilon*1
    epsilon_decay_value = epsilon / (rl_config.end_epsilon_decaying - rl_config.start_epsilon_decaying)

    for episode in range(rl_config.episodes):
        vector_state = matris_to_vector(env.reset(screen=screen))
        episode_reward = 0
        done = False
        if episode % rl_config.show_every == 0:
            render = True
        else:
            render = False
        now = datetime.now()
        while not done:

            if np.random.random() > epsilon:
                action = q_agent.next_action(vector_state)
            else:
                action = random.choice(all_actions)

            new_state, reward, done, _ = env.step(action, render)
            #reward -= get_bumpiness(new_state) * 10 + get_num_holes(new_state) * 5 + get_sum_height(new_state) * 20
            #reward += get_lines_cleared(new_state) * 1000
            reward = - get_height(new_state)*get_uneveness_factor(new_state)
            episode_reward += reward
            vector_state = matris_to_vector(new_state)
            if render:
                env.render()

            if not done:
                max_future_q = q_agent.q_table.get_max_q_value(vector_state)
                current_q = q_agent.q_table.get_q_value(vector_state, action)
                new_q = (1 - rl_config.learning_rate) * current_q + rl_config.learning_rate \
                        * (reward + rl_config.discount * max_future_q)
                q_agent.q_table.set_q_value(vector_state, action, new_q)
            else:
                # Tetris Gameover
                q_agent.q_table.set_q_value(vector_state, action, rl_config.game_over_value)

        if rl_config.end_epsilon_decaying >= episode >= rl_config.start_epsilon_decaying:
            epsilon -= epsilon_decay_value

        print("-" * 30)
        print(f"Episode: {episode}")
        print(f"Episode reward: {episode_reward}")
        print(f"Epsilon: {epsilon}")
        print(f"Episode done in: {datetime.now() - now}")
        print("-" * 30)

    print(f"Q Table: {q_agent.q_table.data}")
    if rl_config.save_agent_after_training:
        save_agent(q_agent, rl_config.agent_file_path)

    env.close()
