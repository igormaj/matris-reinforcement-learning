import pygame


class Actions:
    NO_ACTION = None
    UP = pygame.K_UP
    DOWN = pygame.K_DOWN
    LEFT = pygame.K_LEFT
    RIGHT = pygame.K_RIGHT,
    HARD_DROP = pygame.K_SPACE

    # the following format tup(int,), for some reason. Others are just ints
    # subscriptable right away does not work: pygame.K_RIGHT[0] fails with int is
    # not subscriptable
    RIGHT = RIGHT[0]


action_map = {
    Actions.NO_ACTION: "NO_ACTION",
    Actions.UP: "UP",
    Actions.DOWN: "DOWN",
    Actions.LEFT: "LEFT",
    Actions.RIGHT: "RIGHT",
    Actions.HARD_DROP: "HARD_DROP"
}

all_actions = list(action_map.keys())