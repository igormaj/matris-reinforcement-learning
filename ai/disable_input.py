try:
    from ctypes import windll
except:
    print("Not on windows probably!")


def disable_keyboard():
    try:
        windll.user32.BlockInput(True)  # enable block
    except:
        print("Not on windows! Input disabling failed")


def enable_keyboard():
    try:
        windll.user32.BlockInput(False)  # disable block
    except:
        print("Not on windows! Input enabling failed.")
