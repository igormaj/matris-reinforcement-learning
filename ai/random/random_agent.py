

from ai.abstract_agent import AbstractAgent
from ai.actions import all_actions, action_map
import random


""" Simple random agent who picks his moves randomly """


class RandomAgent(AbstractAgent):

    def __init__(self):
        super(RandomAgent, self).__init__()

    def next_action(self, state):
        action = random.choice(all_actions)
        print("Random agent:", action,action_map[action])
        return action
