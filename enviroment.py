from matris import *
from ai.disable_input import disable_keyboard, enable_keyboard
from ai.reinforcement.vectorization import matris_to_vector


def construct_nightmare(size):
    """
    Constructs background image
    """
    surf = Surface(size)

    boxsize = 8
    bordersize = 1
    vals = '1235'  # only the lower values, for darker colors and greater fear
    arr = pygame.PixelArray(surf)
    for x in range(0, len(arr), boxsize):
        for y in range(0, len(arr[x]), boxsize):

            color = int(''.join([random.choice(vals) + random.choice(vals) for _ in range(3)]), 16)

            for LX in range(x, x + (boxsize - bordersize)):
                for LY in range(y, y + (boxsize - bordersize)):
                    if LX < len(arr) and LY < len(arr[x]):
                        arr[LX][LY] = color
    del arr
    return surf


class Game(object):

    def __init__(self):
        self.clock = None
        self.matris = None
        self.screen = None

    def _init_loop(self, screen):
        self.screen = screen
        clock = pygame.time.Clock()

        self.matris = Matris(screen)

        screen.blit(construct_nightmare(screen.get_size()), (0, 0))

        matris_border = Surface(
            (MATRIX_WIDTH * BLOCKSIZE + BORDERWIDTH * 2, VISIBLE_MATRIX_HEIGHT * BLOCKSIZE + BORDERWIDTH * 2))
        matris_border.fill(BORDERCOLOR)
        screen.blit(matris_border, (MATRIS_OFFSET, MATRIS_OFFSET))

        self.redraw()
        return clock

    def main(self, screen):
        """
        Main loop for game
        Redraws scores and next tetromino each time the loop is passed through
        """
        self.clock = self._init_loop(screen)

        while True:
            try:
                timepassed = self.clock.tick(50)
                if self.matris.update((timepassed / 1000.) if not self.matris.paused else 0):
                    self.redraw()
            except GameOver:
                return

    @staticmethod
    def _simulate_key_press(action):
        if action is None:
            return
        if type(action) is list:
            for act in action:
                new_event = pygame.event.Event(pygame.KEYDOWN, key=act)  # create the event
                pygame.event.post(new_event)  # add the event to the queue
        else:
            new_event = pygame.event.Event(pygame.KEYDOWN, key=action)  # create the event
            pygame.event.post(new_event)  # add the event to the queue

    def reset(self, screen):
        """Resets the enviroment and returns the initial state (inspired by gym)"""
        self.clock = pygame.time.Clock()
        self.screen = screen

        self.matris = Matris(screen)

        screen.blit(construct_nightmare(screen.get_size()), (0, 0))

        matris_border = Surface(
            (MATRIX_WIDTH * BLOCKSIZE + BORDERWIDTH * 2, VISIBLE_MATRIX_HEIGHT * BLOCKSIZE + BORDERWIDTH * 2))
        matris_border.fill(BORDERCOLOR)
        screen.blit(matris_border, (MATRIS_OFFSET, MATRIS_OFFSET))
        return self.matris

    def step(self, action, render=False):
        """Does one step in a game loop
           One step can contain multiple actions
           (see simulate_key_press method)
           returns new_state, reward, done, additional data
           inspired by gym
        """
        previous_score = self.matris.score * 1
        done = False
        try:
            timepassed = self.clock.tick(50) if render else 50  # speeds up the game when it is not showing
            self._simulate_key_press(action=action)
            self.matris.update((timepassed / 1000.))
        except GameOver:
            done = True
        reward = self.matris.score - previous_score
        return self.matris, reward, done, None

    def render(self):
        """Inspired by gym"""
        try:
            self.redraw()
        except:
            pass

    """Closes the enviroment (inspired by gym)"""

    def close(self):
        try:
            self.matris.gameover()
        except:
            pass

    def ai_loop(self, screen, agent, matris_to_vector=matris_to_vector):
        """
        AI loop for game
        Redraws scores and next tetromino each time the loop is passed through
        Allows the agent to automatically play the game
        """
        self.clock = self._init_loop(screen)
        disable_keyboard()
        while True:
            try:
                timepassed = self.clock.tick(50)
                self._simulate_key_press(agent.next_action(matris_to_vector(self.matris)))
                if self.matris.update((timepassed / 1000.) if not self.matris.paused else 0):
                    self.redraw()
            except GameOver:
                enable_keyboard()
                return

    def redraw(self):
        """
        Redraws the information panel and next termoino panel
        """
        if not self.matris.paused:
            self.blit_next_tetromino(self.matris.surface_of_next_tetromino)
            self.blit_info()

            self.matris.draw_surface()

        pygame.display.flip()

    def blit_info(self):
        """
        Draws information panel
        """
        textcolor = (255, 255, 255)
        font = pygame.font.Font(None, 30)
        width = (WIDTH - (MATRIS_OFFSET + BLOCKSIZE * MATRIX_WIDTH + BORDERWIDTH * 2)) - MATRIS_OFFSET * 2

        def renderpair(text, val):
            text = font.render(text, True, textcolor)
            val = font.render(str(val), True, textcolor)

            surf = Surface((width, text.get_rect().height + BORDERWIDTH * 2), pygame.SRCALPHA, 32)

            surf.blit(text, text.get_rect(top=BORDERWIDTH + 10, left=BORDERWIDTH + 10))
            surf.blit(val, val.get_rect(top=BORDERWIDTH + 10, right=width - (BORDERWIDTH + 10)))
            return surf

        # Resizes side panel to allow for all information to be display there.
        scoresurf = renderpair("Score", self.matris.score)
        levelsurf = renderpair("Level", self.matris.level)
        linessurf = renderpair("Lines", self.matris.lines)
        combosurf = renderpair("Combo", "x{}".format(self.matris.combo))

        height = 20 + (levelsurf.get_rect().height +
                       scoresurf.get_rect().height +
                       linessurf.get_rect().height +
                       combosurf.get_rect().height)

        # Colours side panel
        area = Surface((width, height))
        area.fill(BORDERCOLOR)
        area.fill(BGCOLOR, Rect(BORDERWIDTH, BORDERWIDTH, width - BORDERWIDTH * 2, height - BORDERWIDTH * 2))

        # Draws side panel
        area.blit(levelsurf, (0, 0))
        area.blit(scoresurf, (0, levelsurf.get_rect().height))
        area.blit(linessurf, (0, levelsurf.get_rect().height + scoresurf.get_rect().height))
        area.blit(combosurf,
                  (0, levelsurf.get_rect().height + scoresurf.get_rect().height + linessurf.get_rect().height))

        self.screen.blit(area, area.get_rect(bottom=HEIGHT - MATRIS_OFFSET, centerx=TRICKY_CENTERX))

    def blit_next_tetromino(self, tetromino_surf):
        """
        Draws the next tetromino in a box to the side of the board
        """
        area = Surface((BLOCKSIZE * 5, BLOCKSIZE * 5))
        area.fill(BORDERCOLOR)
        area.fill(BGCOLOR,
                  Rect(BORDERWIDTH, BORDERWIDTH, BLOCKSIZE * 5 - BORDERWIDTH * 2, BLOCKSIZE * 5 - BORDERWIDTH * 2))

        areasize = area.get_size()[0]
        tetromino_surf_size = tetromino_surf.get_size()[0]
        # ^^ I'm assuming width and height are the same

        center = areasize / 2 - tetromino_surf_size / 2
        area.blit(tetromino_surf, (center, center))

        self.screen.blit(area, area.get_rect(top=MATRIS_OFFSET, centerx=TRICKY_CENTERX))
