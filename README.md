MaTris Reinforcement learning
======

Forked from: https://github.com/Uglemat/MaTris 

The aim of the project is to allow for training and/or testing
of different reinforcement agents which play the MaTris implementation of the Tetris game.

Requirements:
1. python3
2. Tensorflow
3. Keras
4. Pygame (for game rendering)

Run `python main.py` (or `python3 main.py`) to start the game. This will start the game with the default `config.yaml`
located in the root game directory.
The config file consists of key:value pairs:
    
    discount: discount factor
    end_epsilon_decaying: when (on which episode) to end epsilon decaying (if the algorithm uses epsilon greedy)
    episodes: number of episodes
    epsilon: (0-1) if the algorithm uses epsilon greedy
    game_over_value: if the agent reward function uses game over value
    learning_rate: self explanatory

    load_agent: (True/False) whether to load the agent from the path
    agent_file_path: relative or absolute path
    agent_type: 'name_of_the_agent' -> currently supported agents are: 
    `q_agent`, `double_q_agent`, `gd_agent`, `deep_q_agent`, `reinforce_agent`

    save_agent_after_training: self explanatory
    show_every: render every xth episode
    start_epsilon_decaying: on which episode to start epsilon decaying(if the agent uses epsilon greedy )

It is possible to specify the config file path: `python main.py <path-to-file>`.
After running the command a GUI program will start with the following menu:

![](images/menu.PNG) 


It is possible to play the game, test a random agent and train and test a reinforcement learning agent
(the tested agent depends on the loaded config).

The agents with best performances (scores) are the following: `gd_agent` and `deep_q_agent`.
